# README #

This is a cloudformation script to bring up the infrastructure to host a 'Hello world' application from a Git repository. It uses AWS EC2 to host the application.

### What this script contains?

* This script contains how to create a EC2, install git, 
  install http server and attach a load balancer.
* It also uses a VPC, Subnet, Security Group, Elastic Load Balancer,
  Launch Configuration, Auto Scaling group with Simple Scaling Policy, Internet Gateway, Route table.
* The output of the CloudFormation template is a URL, when visiting 
  the URL in the browser you should see 'Hello Word' displayed from a Git repository.
#
### How to run the template in AWS Console? 

* Clone Git repository on local machine
* Login to your AWS account.
* Go to CloudFormation service and create stack.
* Select 'Template is ready' and 'Upload a template file'
* Choose 'hello_world_ec2_infra.json' file. Click 'Next'
* Enter Stack name , Select KeyPair name. Click 'Next'
* Select IAM role for Cloudformation to create or delete resources 
  in the stack. If you don't choose a role, Cloudformation uses 
  permissions based on your user credentials. Click 'Create Stack'
* It might take few minutes to create resources. 
* Once status of the Stack is 'CREATE_COMPLETE', you can go to EC2 service 
  and check Instance status. It may take some time for the Auto Scaling group 
  to report instance as healthy.
* Once the instance is healthy, Go to 'Output' in CloudFormation and Click on the URL of the Load Balancer  
#  
### How to run using AWS Command Line Interface (CLI)?
* Make sure that you set up your AWS CLI using "aws configure" command.
* On Terminal (Mac of Linux) or Command Prompt (Windows) and change current directory 
  to "helloworld-infra" run below aws cloudformation command:
* Note: Please make sure that keypair name you provide in the command exists for the region  

#
```
aws cloudformation create-stack --stack-name hello-world --template-body file://hello_world_ec2_infra.json --region us-east-1 --parameters ParameterKey=KeyName,ParameterValue=keypair name
```
* You should receive "StackId" if above command is successful.

#
### How to get the dynamic URL of the Hello-World website?

* Go to 'Output' tab of the Stack and make sure the URL of the Load Balancer is displayed.
* Go to the URL. You should see 'Hello world' displayed.
#
### How to Delete/Cleanup the infrastructure using AWS Console?

* Go to CloudFormation service, Select the Stack and Click on 
  'Delete' button. The popup will appear to confirm the Delete 
  the stack. Click on 'Delete stack' button.
* It will take some time to delete all the resources in the stack.
  During that time it will show 'DELETE_IN_PROGRESS' in the status.
#  
### How to Delete/Cleanup the infrastructure using AWS Command Line Interface (CLI)?

```
aws cloudformation delete-stack --stack-name  hello-world --region us-east-1
```
* It will take some time to delete all the resources in the stack.
* Above command will not give any kind of confirmation status 
  whether the stack is deleted or not, but you can check the current 
  status using below command:
```  
aws cloudformation describe-stacks --stack-name hello-world --region us-east-1
```
* Above command will give validation error stating that the stack with id "hello-world" does not exists

#
### The source code

* https://prakashpateldev@bitbucket.org/prakashpateldev/helloworld-infra.git
* Author : Prakash Patel




